package angelectro.com.vktest.widjets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import angelectro.com.vktest.R;
import angelectro.com.vktest.activity.ItemNewsActivity;
import angelectro.com.vktest.activity.NewsActivity;
import angelectro.com.vktest.content.News;
import angelectro.com.vktest.presenter.NewsRecyclerPresenter;
import angelectro.com.vktest.view.UpdateView;

/**
 * Created by Zahit Talipov on 23.04.2016.
 */
public class NewsRecyclerView extends RecyclerView implements UpdateView, NewsActivity.OnClickRepeat {
    private static String startFrom;
    NewRecyclerAdapter adapter;
    boolean isRefreshNext = false;
    boolean isRefresh = false;
    OnItemClickListener listener = (view, position) -> showItem(position);
    private ProgressBar mProgressBar;
    private LinearLayout mErrorLayout;
    private NewsRecyclerPresenter mPresenter;
    private NewsSwipeRefreshLayout mRefreshLayout;
    private LinearLayoutManager manager;

    public NewsRecyclerView(Context context) {
        super(context);
    }

    public NewsRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewsRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (NewRecyclerAdapter.newsList.isEmpty()) {
            mPresenter.refresh();
            isRefresh = true;
            mProgressBar.setVisibility(VISIBLE);
        }
    }

    public void setProgressBar(ProgressBar mProgressBar) {
        this.mProgressBar = mProgressBar;
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void init() {
        adapter = new NewRecyclerAdapter(getContext());
        setAdapter(adapter);
        adapter.setOnItemClickListener(listener);
        manager = new LinearLayoutManager(getContext());
        setLayoutManager(manager);
        mPresenter = new NewsRecyclerPresenter(this);
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager manager = (LinearLayoutManager) getLayoutManager();
                int position = manager.findLastVisibleItemPosition();
                int updatePosition = getAdapter().getItemCount() - 3;
                if (position >= updatePosition && !isRefreshNext) {
                    isRefreshNext = true;
                    mPresenter.refresh(startFrom);
                }
            }
        });
    }

    @Override
    public void updateAdapter(News news) {
        mProgressBar.setVisibility(INVISIBLE);
        startFrom = news.getResponse().getNext_from();
        adapter.update(news, isRefreshNext);
        isRefreshNext = false;
        isRefresh = false;
        mErrorLayout.setVisibility(GONE);
        mRefreshLayout.stopRefreshing();
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(Throwable throwable) {
        throwable.printStackTrace();
        mProgressBar.setVisibility(INVISIBLE);
        mRefreshLayout.stopRefreshing();
        if (!isRefresh&&mErrorLayout.getVisibility()!=VISIBLE)
            Snackbar.make(this, getContext().getText(R.string.text_error_no_connection), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Повторить", new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(isRefreshNext)
                            mPresenter.refresh(startFrom);
                            else
                                mPresenter.refresh();
                        }
                    }).show();
        else
            mErrorLayout.setVisibility(VISIBLE);
        isRefreshNext = false;
        isRefresh = false;
    }

    public void setRefreshLayout(NewsSwipeRefreshLayout mRefreshLayout) {
        this.mRefreshLayout = mRefreshLayout;
        mRefreshLayout.setRecyclerView(this);
    }

    private void showItem(int position) {
        Intent intent = new Intent(getContext(), ItemNewsActivity.class);
        intent.putExtra(ItemNewsActivity.POSITION_KEY, position);
        getContext().startActivity(intent);
    }


    public void setErrorLayout(LinearLayout mErrorLayout) {
        this.mErrorLayout = mErrorLayout;
    }

    @Override
    public void onRepeat() {
        isRefresh = true;
        mErrorLayout.setVisibility(GONE);
        mPresenter.refresh();
    }

    public interface OnItemClickListener {
        public void onClick(View view, int position);
    }

}
