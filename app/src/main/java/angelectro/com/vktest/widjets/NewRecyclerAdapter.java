package angelectro.com.vktest.widjets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import angelectro.com.vktest.BuilderNewsItem;
import angelectro.com.vktest.FormatterTools;
import angelectro.com.vktest.R;
import angelectro.com.vktest.content.Attachment;
import angelectro.com.vktest.content.CopyHistory;
import angelectro.com.vktest.content.Group;
import angelectro.com.vktest.content.Item;
import angelectro.com.vktest.content.News;
import angelectro.com.vktest.content.Photo;
import angelectro.com.vktest.content.Profile;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 23.04.2016.
 */
public class NewRecyclerAdapter extends RecyclerView.Adapter<NewRecyclerAdapter.Holder> {
    public static List<Item> newsList = new ArrayList<>();
    public static News mNews;
    NewsRecyclerView.OnItemClickListener onItemClickListener;
    private Context mContext;

    public NewRecyclerAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnItemClickListener(NewsRecyclerView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false));
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Item item = newsList.get(position);
        BuilderNewsItem.build(holder,item,mContext);
        Log.d("name", item.toString());
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public void update(News news, boolean isNext) {
        if (isNext) {
            newsList.addAll(news.getResponse().getItems());
            mNews.getResponse().getGroups().addAll(news.getResponse().getGroups());
            mNews.getResponse().getProfiles().addAll(news.getResponse().getProfiles());
        } else {
            mNews = news;
            newsList.clear();
            newsList.addAll(news.getResponse().getItems());
        }
        notifyDataSetChanged();
    }


    public class Holder extends RecyclerView.ViewHolder {
        @Bind(R.id.photo_icon)
        public ImageView mPhoto_icon;
        @Bind(R.id.name)
        public TextView mName;
        @Bind(R.id.publishDate)
        public TextView mPublishDate;
        @Bind(R.id.mTtextViewLike)
        public TextView mTextViewLike;
        @Bind(R.id.mTextViewRepost)
        public TextView mTextViewRepost;
        @Bind(R.id.rootLinearLayout)
        public LinearLayout mRootView;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> onItemClickListener.onClick(v, getLayoutPosition()));
        }
        }
}
