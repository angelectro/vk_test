package angelectro.com.vktest.widjets;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

import angelectro.com.vktest.presenter.NewsRecyclerPresenter;

/**
 * Created by Zahit Talipov on 23.04.2016.
 */
public class NewsSwipeRefreshLayout extends SwipeRefreshLayout {
    NewsRecyclerView mNewsRecyclerView;
    NewsRecyclerPresenter mPresenter;

    public NewsSwipeRefreshLayout(Context context) {
        super(context);
    }

    public NewsSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setRecyclerView(NewsRecyclerView recyclerView) {
        mNewsRecyclerView = recyclerView;
        mPresenter = new NewsRecyclerPresenter(mNewsRecyclerView);
        init();
    }

    private void init() {
        setOnRefreshListener(mPresenter::refresh);
    }

    public void stopRefreshing() {
        setRefreshing(false);
    }
}
