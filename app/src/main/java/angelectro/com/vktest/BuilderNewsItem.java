package angelectro.com.vktest;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.List;

import angelectro.com.vktest.activity.ItemNewsActivity;
import angelectro.com.vktest.content.Attachment;
import angelectro.com.vktest.content.CopyHistory;
import angelectro.com.vktest.content.Group;
import angelectro.com.vktest.content.Item;
import angelectro.com.vktest.content.Photo;
import angelectro.com.vktest.content.Profile;
import angelectro.com.vktest.widjets.NewRecyclerAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 28.04.2016.
 */
public class BuilderNewsItem {

    static Context mContext;


    public static void build(NewRecyclerAdapter.Holder holder, Item item, Context context) {
        holder.mRootView.removeAllViews();
        mContext = context;
        showHeaderAndFooterPost(holder, item);
        showBodyPost(item, holder);
    }

    public static void build(ItemNewsActivity activity, Item item) {
        activity.mRootView.removeAllViews();
        mContext = activity;
        showHeaderAndFooterPost(activity, item);
        showBodyPost(item, activity);
    }

    private static Profile findProfile(int id) {
        Iterator<Profile> profileIterator = NewRecyclerAdapter.mNews.getResponse().getProfiles().iterator();
        while (profileIterator.hasNext()) {
            Profile profile = profileIterator.next();
            if (profile.getId() == id)
                return profile;
        }
        return null;
    }

    private static Group findGroup(int id) {
        Iterator<Group> groupIterator = NewRecyclerAdapter.mNews.getResponse().getGroups().iterator();
        id = Math.abs(id);
        while (groupIterator.hasNext()) {
            Group group = groupIterator.next();
            if (group.getId() == id)
                return group;
        }
        return null;
    }

    private static void showAttachments(List<Attachment> attachments, LinearLayout view, boolean isFirst) {
        if (!attachments.isEmpty()) {
            for (Attachment attachment : attachments) {
                if (isTypePhotos(attachment)) {
                    ImageView imageView = (ImageView) LayoutInflater.from(mContext).inflate(R.layout.post_body_image, null);
                    view.addView(imageView);

                    imageView.setVisibility(View.VISIBLE);
                    Photo photoAttachment = attachment.getPhoto();
                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setSize(photoAttachment.getWidth(), photoAttachment.getHeight());

                    gradientDrawable.setColor(ContextCompat.getColor(mContext, R.color.colorPlaceholder));
                    Picasso.with(mContext)
                            .load(photoAttachment.getPhoto_604())
                            .placeholder(gradientDrawable)
                            .into(imageView);
                }
                if (isFirst)
                    break;
            }
        }
    }

    private static void showBodyPost(Item item, NewRecyclerAdapter.Holder holder) {
        showAttachments(item.getAttachments(), holder.mRootView, true);
        if (!item.getCopy_history().isEmpty()) {
            LinearLayout view = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.post_child, null);
            HolderChild holderChild = new HolderChild(view);
            CopyHistory copyHistory = item.getCopy_history().get(0);
            int itemSource = copyHistory.getOwner_id();
            String name;
            String photo;
            if (itemSource > 0) {
                Profile profile = findProfile(itemSource);
                name = String.format("%s %s", profile.getLast_name(), profile.getFirst_name());
                photo = profile.getPhoto_50();
            } else {
                Group group = findGroup(itemSource);
                name = group.getName();
                photo = group.getPhoto_50();
            }
            holderChild.mName.setText(name);
            holderChild.mPublishDate.setText(FormatterTools.getDateString(copyHistory.getDate()));
            if (!TextUtils.isEmpty(copyHistory.getText())) {
                TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.post_text, null);
                textView.setText(Html.fromHtml(FormatterTools.headerFormat(copyHistory.getText())));
                holderChild.mRootLinearLayout.addView(textView);
            }
            Picasso.with(mContext)
                    .load(photo)
                    .placeholder(new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorPlaceholder)))
                    .into(holderChild.mPhoto_icon);
            showAttachments(copyHistory.getAttachments(), holderChild.mRootLinearLayout,true);
            holder.mRootView.addView(view);
        }
    }

    private static void showHeaderAndFooterPost(NewRecyclerAdapter.Holder holder, Item item) {
        int itemSource = item.getSource_id();
        String name;
        String photo;
        if (itemSource > 0) {
            Profile profile = findProfile(itemSource);
            name = String.format("%s %s", profile.getLast_name(), profile.getFirst_name());
            photo = profile.getPhoto_50();
        } else {
            Group group = findGroup(itemSource);
            name = group.getName();
            photo = group.getPhoto_50();
        }
        String text = item.getText();
        holder.mName.setText(name);
        holder.mPublishDate.setText(FormatterTools.getDateString(item.getDate()));
        holder.mTextViewRepost.setText(String.valueOf(item.getReposts().getCount()));
        holder.mTextViewLike.setText(String.valueOf(item.getLikes().getCount()));

        if (!TextUtils.isEmpty(text)) {
            TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.post_text, null);
            textView.setText(Html.fromHtml(FormatterTools.headerFormat(text)));
            holder.mRootView.addView(textView);
        }
        Picasso.with(mContext)
                .load(photo)
                .placeholder(new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorPlaceholder)))
                .into(holder.mPhoto_icon);
    }

    private static boolean isTypePhotos(Attachment attachment) {
        return attachment.getType().contains("photo") || attachment.getType().contains("posted_photo");
    }

    private static void showBodyPost(Item item, ItemNewsActivity activity) {
        showAttachments(item.getAttachments(), activity.mRootView,false);
        if (!item.getCopy_history().isEmpty()) {
            LinearLayout view = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.post_child, null);
            HolderChild holderChild = new HolderChild(view);
            CopyHistory copyHistory = item.getCopy_history().get(0);
            int itemSource = copyHistory.getOwner_id();
            String name;
            String photo;
            if (itemSource > 0) {
                Profile profile = findProfile(itemSource);
                name = String.format("%s %s", profile.getLast_name(), profile.getFirst_name());
                photo = profile.getPhoto_50();
            } else {
                Group group = findGroup(itemSource);
                name = group.getName();
                photo = group.getPhoto_50();
            }
            holderChild.mName.setText(name);
            holderChild.mPublishDate.setText(FormatterTools.getDateString(copyHistory.getDate()));
            if (!TextUtils.isEmpty(copyHistory.getText())) {
                TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.post_text, null);
                textView.setText(Html.fromHtml(FormatterTools.headerFormat(copyHistory.getText())));
                holderChild.mRootLinearLayout.addView(textView);
            }
            Picasso.with(mContext)
                    .load(photo)
                    .placeholder(new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorPlaceholder)))
                    .into(holderChild.mPhoto_icon);
            showAttachments(copyHistory.getAttachments(), holderChild.mRootLinearLayout,true);
            activity.mRootView.addView(view);
        }
    }

    private static void showHeaderAndFooterPost(ItemNewsActivity activity, Item item) {
        int itemSource = item.getSource_id();
        String name;
        String photo;
        if (itemSource > 0) {
            Profile profile = findProfile(itemSource);
            name = String.format("%s %s", profile.getLast_name(), profile.getFirst_name());
            photo = profile.getPhoto_50();
        } else {
            Group group = findGroup(itemSource);
            name = group.getName();
            photo = group.getPhoto_50();
        }
        String text = item.getText();
        activity.mName.setText(name);
        activity.mPublishDate.setText(FormatterTools.getDateString(item.getDate()));
        activity.mTextViewRepost.setText(String.valueOf(item.getReposts().getCount()));
        activity.mTextViewLike.setText(String.valueOf(item.getLikes().getCount()));

        if (!TextUtils.isEmpty(text)) {
            TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.post_text, null);
            textView.setText(Html.fromHtml(text));
            activity.mRootView.addView(textView);
        }
        Picasso.with(mContext)
                .load(photo)
                .placeholder(new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorPlaceholder)))
                .into(activity.mPhoto_icon);
    }

    public static class HolderChild extends RecyclerView.ViewHolder {
        @Bind(R.id.photo_icon)
        ImageView mPhoto_icon;
        @Bind(R.id.name)
        TextView mName;
        @Bind(R.id.publishDate)
        TextView mPublishDate;
        @Bind(R.id.rootLinearLayout)
        LinearLayout mRootLinearLayout;

        public HolderChild(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
