package angelectro.com.vktest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import angelectro.com.vktest.R;
import angelectro.com.vktest.dialogs.ErrorDialog;
import angelectro.com.vktest.dialogs.LoadingDialog;
import angelectro.com.vktest.presenter.LoginPresenter;
import angelectro.com.vktest.view.LoginView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginView {
    @Bind(R.id.editTextLogin)
    EditText editTextLogin;
    @Bind(R.id.editTextPassword)
    EditText editTextPassword;

    LoadingDialog loadingDialog;
    ErrorDialog errorDialog;

    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this);
        loginPresenter.checkIsAuthorized();
    }


    @Override
    public void showError(Throwable throwable) {
        errorDialog = ErrorDialog.createErrorDialog(throwable);
        errorDialog.show(getFragmentManager());
        Log.d("error", throwable.getMessage());
    }

    @Override
    public void showLoading() {
        loadingDialog = new LoadingDialog();
        loadingDialog.show(getFragmentManager());
    }

    @Override
    public void hideLoading() {
        loadingDialog.dismiss();
    }

    @Override
    public void openNewsActivity() {
        startActivity(new Intent(this, NewsActivity.class));
        finish();
    }

    public void signInClick(View view) {
        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();
        loginPresenter.authorization(login, password);
    }
}
