package angelectro.com.vktest.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import angelectro.com.vktest.BuilderNewsItem;
import angelectro.com.vktest.R;
import angelectro.com.vktest.content.Item;
import angelectro.com.vktest.widjets.NewRecyclerAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 27.04.2016.
 */
public class ItemNewsActivity extends AppCompatActivity{
    public static String POSITION_KEY = "position";
    @Bind(R.id.photo_icon)
    public ImageView mPhoto_icon;
    @Bind(R.id.name)
    public TextView mName;
    @Bind(R.id.publishDate)
    public TextView mPublishDate;
    @Bind(R.id.mTtextViewLike)
    public TextView mTextViewLike;
    @Bind(R.id.mTextViewRepost)
    public TextView mTextViewRepost;
    @Bind(R.id.rootLinearLayout)
    public LinearLayout mRootView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(R.layout.activity_item,null);
        setContentView(view);
        ButterKnife.bind(this);
        mName.setMaxLines(5);
        int position = getIntent().getIntExtra(POSITION_KEY,0);
        Item item=NewRecyclerAdapter.newsList.get(position);
        BuilderNewsItem.build(this,item);
    }

}
