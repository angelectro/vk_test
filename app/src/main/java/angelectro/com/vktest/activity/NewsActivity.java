package angelectro.com.vktest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import angelectro.com.vktest.R;
import angelectro.com.vktest.Settings;
import angelectro.com.vktest.widjets.NewsRecyclerView;
import angelectro.com.vktest.widjets.NewsSwipeRefreshLayout;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Zahit Talipov on 24.04.2016.
 */
public class NewsActivity extends AppCompatActivity {
    @Bind(R.id.news_recycler)
    NewsRecyclerView newsRecyclerView;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.news_refresh)
    NewsSwipeRefreshLayout refreshLayout;
    @Bind(R.id.errorLayout)
    LinearLayout mErrorLinearLayout;
    OnClickRepeat onClickRepeat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        newsRecyclerView.setProgressBar(progressBar);
        newsRecyclerView.setRefreshLayout(refreshLayout);
        newsRecyclerView.setErrorLayout(mErrorLinearLayout);
        onClickRepeat = newsRecyclerView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout: {
                Settings.logout();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void clickRepeat(View view) {
        onClickRepeat.onRepeat();
    }

    public interface OnClickRepeat {
        public void onRepeat();
    }
}
