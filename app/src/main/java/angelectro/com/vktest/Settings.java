package angelectro.com.vktest;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import angelectro.com.vktest.content.Authorization;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public class Settings {
    private static Context mContext;
    private static String NAME = "angelectro.com";
    private static String TOKEN = "token";
    private static String USER_ID = "user_id";


    public static void init(@NonNull Context context) {
        mContext = context;
    }

    public static void save(@NonNull Authorization authorization) {
        SharedPreferences.Editor edit = getShared().edit();
        edit.putString(TOKEN,authorization.getAccess_token());
        edit.putString(USER_ID,authorization.getUser_id());
        edit.commit();
        Log.d("token", authorization.getAccess_token());
    }

    public static void logout(){
        SharedPreferences.Editor edit = getShared().edit();
        edit.remove(TOKEN);
        edit.remove(USER_ID);
        edit.commit();
    }

    public static String getToken() {
         return getShared().getString(TOKEN,"");
    }

    public static String getUserId() {
        return getShared().getString(USER_ID,"");
    }

    private static SharedPreferences getShared() {
        return mContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }
}
