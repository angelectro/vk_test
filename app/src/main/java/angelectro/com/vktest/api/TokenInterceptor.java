package angelectro.com.vktest.api;

import android.text.TextUtils;

import java.io.IOException;

import angelectro.com.vktest.Settings;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by Zahit Talipov on 23.04.2016.
 */
public class TokenInterceptor implements Interceptor {
    private static String TOKEN_KEY = "access_token";
    private static String VERSION_KEY = "v";

    private TokenInterceptor() {
    }

    public static TokenInterceptor getInstance() {
        return new TokenInterceptor();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = Settings.getToken();
        if (TextUtils.isEmpty(token)) {
            return chain.proceed(chain.request());
        }
        HttpUrl httpUrl = chain
                .request()
                .url()
                .newBuilder()
                .addQueryParameter(TOKEN_KEY, token)
                .addQueryParameter(VERSION_KEY,"5.52")
                .build();
        Response response = chain
                .proceed(chain.request().newBuilder().url(httpUrl).build());
        return response;
    }
}
