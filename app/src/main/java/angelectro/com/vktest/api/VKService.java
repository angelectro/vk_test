package angelectro.com.vktest.api;

import angelectro.com.vktest.content.Authorization;
import angelectro.com.vktest.content.News;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public interface VKService {

    @GET("token")
    public Observable<Authorization> getToken(
            @Query("grant_type") String grantType,
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("username") String username,
            @Query("password") String password
    );

    @GET("method/newsfeed.get")
    public Observable<News> getNews(@Query("filters") String filters);

    @GET("method/newsfeed.get")
    public Observable<News> getNewsNext(
            @Query("start_from") String nextFrom,
            @Query("filters") String filters
    );

}
