package angelectro.com.vktest.api;

import angelectro.com.vktest.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public class ApiProvider {


    public static VKService getProviderVKService() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(OkHttp.getOkHttpClient())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(VKService.class);
    }
    public static VKService getProviderVKServiceAuthorization() {

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT_AUTHORIZATION)
                .client(OkHttp.getOkHttpClient())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(VKService.class);

    }
}
