
package angelectro.com.vktest.content;


import com.google.gson.annotations.SerializedName;

public class Group {
    private int id;
    @SerializedName("name")
    private String name;
    private String screen_name;
    private Integer is_closed;
    private String type;
    private Integer is_admin;
    private Integer is_member;
    private String photo_50;
    private String photo_100;
    private String photo_200;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The screen_name
     */
    public String getScreen_name() {
        return screen_name;
    }

    /**
     * 
     * @param screen_name
     *     The screen_name
     */
    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    /**
     * 
     * @return
     *     The is_closed
     */
    public Integer getIs_closed() {
        return is_closed;
    }

    /**
     * 
     * @param is_closed
     *     The is_closed
     */
    public void setIs_closed(Integer is_closed) {
        this.is_closed = is_closed;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The is_admin
     */
    public Integer getIs_admin() {
        return is_admin;
    }

    /**
     * 
     * @param is_admin
     *     The is_admin
     */
    public void setIs_admin(Integer is_admin) {
        this.is_admin = is_admin;
    }

    /**
     * 
     * @return
     *     The is_member
     */
    public Integer getIs_member() {
        return is_member;
    }

    /**
     * 
     * @param is_member
     *     The is_member
     */
    public void setIs_member(Integer is_member) {
        this.is_member = is_member;
    }

    /**
     * 
     * @return
     *     The photo_50
     */
    public String getPhoto_50() {
        return photo_50;
    }

    /**
     * 
     * @param photo_50
     *     The photo_50
     */
    public void setPhoto_50(String photo_50) {
        this.photo_50 = photo_50;
    }

    /**
     * 
     * @return
     *     The photo_100
     */
    public String getPhoto_100() {
        return photo_100;
    }

    /**
     * 
     * @param photo_100
     *     The photo_100
     */
    public void setPhoto_100(String photo_100) {
        this.photo_100 = photo_100;
    }

    /**
     * 
     * @return
     *     The photo_200
     */
    public String getPhoto_200() {
        return photo_200;
    }

    /**
     * 
     * @param photo_200
     *     The photo_200
     */
    public void setPhoto_200(String photo_200) {
        this.photo_200 = photo_200;
    }

}
