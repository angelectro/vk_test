package angelectro.com.vktest.content;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zahit Talipov on 27.04.2016.
 */
public class CopyHistory {
    private Integer id;
    private Integer owner_id;
    private Integer from_id;
    private Integer date;
    private String post_type;
    private String text;
    private List<Attachment> attachments = new ArrayList<Attachment>();
    private PostSource post_source;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The owner_id
     */
    public Integer getOwner_id() {
        return owner_id;
    }

    /**
     *
     * @param owner_id
     * The owner_id
     */
    public void setOwner_id(Integer owner_id) {
        this.owner_id = owner_id;
    }

    /**
     *
     * @return
     * The from_id
     */
    public Integer getFrom_id() {
        return from_id;
    }

    /**
     *
     * @param from_id
     * The from_id
     */
    public void setFrom_id(Integer from_id) {
        this.from_id = from_id;
    }

    /**
     *
     * @return
     * The date
     */
    public Integer getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(Integer date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The post_type
     */
    public String getPost_type() {
        return post_type;
    }

    /**
     *
     * @param post_type
     * The post_type
     */
    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The attachments
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     *
     * @param attachments
     * The attachments
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     *
     * @return
     * The post_source
     */
    public PostSource getPost_source() {
        return post_source;
    }

    /**
     *
     * @param post_source
     * The post_source
     */
    public void setPost_source(PostSource post_source) {
        this.post_source = post_source;
    }

}