
package angelectro.com.vktest.content;


public class Likes_ {

    private Integer user_likes;
    private Integer count;

    /**
     * 
     * @return
     *     The user_likes
     */
    public Integer getUser_likes() {
        return user_likes;
    }

    /**
     * 
     * @param user_likes
     *     The user_likes
     */
    public void setUser_likes(Integer user_likes) {
        this.user_likes = user_likes;
    }

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

}
