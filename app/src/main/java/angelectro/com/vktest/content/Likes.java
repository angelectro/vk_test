
package angelectro.com.vktest.content;


public class Likes {

    private Integer count;
    private Integer user_likes;
    private Integer can_like;
    private Integer can_publish;

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The user_likes
     */
    public Integer getUser_likes() {
        return user_likes;
    }

    /**
     * 
     * @param user_likes
     *     The user_likes
     */
    public void setUser_likes(Integer user_likes) {
        this.user_likes = user_likes;
    }

    /**
     * 
     * @return
     *     The can_like
     */
    public Integer getCan_like() {
        return can_like;
    }

    /**
     * 
     * @param can_like
     *     The can_like
     */
    public void setCan_like(Integer can_like) {
        this.can_like = can_like;
    }

    /**
     * 
     * @return
     *     The can_publish
     */
    public Integer getCan_publish() {
        return can_publish;
    }

    /**
     * 
     * @param can_publish
     *     The can_publish
     */
    public void setCan_publish(Integer can_publish) {
        this.can_publish = can_publish;
    }

}
