
package angelectro.com.vktest.content;


public class Profile {

    private Integer id;
    private String first_name;
    private String last_name;
    private Integer sex;
    private String screen_name;
    private String photo_50;
    private String photo100;
    private Integer online;
    private String online_app;
    private Integer online_mobile;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * 
     * @param first_name
     *     The first_name
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * 
     * @return
     *     The last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * 
     * @param last_name
     *     The last_name
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * 
     * @return
     *     The sex
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * 
     * @param sex
     *     The sex
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * 
     * @return
     *     The screen_name
     */
    public String getScreen_name() {
        return screen_name;
    }

    /**
     * 
     * @param screen_name
     *     The screen_name
     */
    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    /**
     * 
     * @return
     *     The photo_50
     */
    public String getPhoto_50() {
        return photo_50;
    }

    /**
     * 
     * @param photo_50
     *     The photo_50
     */
    public void setPhoto_50(String photo_50) {
        this.photo_50 = photo_50;
    }

    /**
     * 
     * @return
     *     The photo100
     */
    public String getPhoto100() {
        return photo100;
    }

    /**
     * 
     * @param photo100
     *     The photo_100
     */
    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    /**
     * 
     * @return
     *     The online
     */
    public Integer getOnline() {
        return online;
    }

    /**
     * 
     * @param online
     *     The online
     */
    public void setOnline(Integer online) {
        this.online = online;
    }

    /**
     * 
     * @return
     *     The online_app
     */
    public String getOnline_app() {
        return online_app;
    }

    /**
     * 
     * @param online_app
     *     The online_app
     */
    public void setOnline_app(String online_app) {
        this.online_app = online_app;
    }

    /**
     * 
     * @return
     *     The online_mobile
     */
    public Integer getOnline_mobile() {
        return online_mobile;
    }

    /**
     * 
     * @param online_mobile
     *     The online_mobile
     */
    public void setOnline_mobile(Integer online_mobile) {
        this.online_mobile = online_mobile;
    }

}
