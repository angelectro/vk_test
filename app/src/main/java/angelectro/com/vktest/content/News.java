
package angelectro.com.vktest.content;


public class News {

    private ResponseNews response;

    /**
     * 
     * @return
     *     The response
     */
    public ResponseNews getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(ResponseNews response) {
        this.response = response;
    }

}
