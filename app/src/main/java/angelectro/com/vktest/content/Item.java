
package angelectro.com.vktest.content;

import java.util.ArrayList;
import java.util.List;

public class Item {

    private String type;
    private Integer source_id;
    private Integer date;
    private Friends friends;
    private Integer post_id;
    private String post_type;
    private String text;
    private List<Attachment> attachments = new ArrayList<Attachment>();
    private List<CopyHistory> copy_history = new ArrayList<CopyHistory>();
    private PostSource post_source;
    private Comments comments;
    private Likes likes;
    private Reposts reposts;
    private Photos photos;

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The source_id
     */
    public Integer getSource_id() {
        return source_id;
    }

    /**
     * 
     * @param source_id
     *     The source_id
     */
    public void setSource_id(Integer source_id) {
        this.source_id = source_id;
    }

    /**
     * 
     * @return
     *     The date
     */
    public Integer getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(Integer date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The friends
     */
    public Friends getFriends() {
        return friends;
    }

    /**
     * 
     * @param friends
     *     The friends
     */
    public void setFriends(Friends friends) {
        this.friends = friends;
    }

    /**
     * 
     * @return
     *     The post_id
     */
    public Integer getPost_id() {
        return post_id;
    }

    /**
     * 
     * @param post_id
     *     The post_id
     */
    public void setPost_id(Integer post_id) {
        this.post_id = post_id;
    }

    /**
     * 
     * @return
     *     The post_type
     */
    public String getPost_type() {
        return post_type;
    }

    /**
     * 
     * @param post_type
     *     The post_type
     */
    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The attachments
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * 
     * @param attachments
     *     The attachments
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * 
     * @return
     *     The post_source
     */
    public PostSource getPost_source() {
        return post_source;
    }

    /**
     * 
     * @param post_source
     *     The post_source
     */
    public void setPost_source(PostSource post_source) {
        this.post_source = post_source;
    }

    /**
     * 
     * @return
     *     The comments
     */
    public Comments getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    public void setComments(Comments comments) {
        this.comments = comments;
    }

    /**
     * 
     * @return
     *     The likes
     */
    public Likes getLikes() {
        return likes;
    }

    /**
     * 
     * @param likes
     *     The likes
     */
    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    /**
     * 
     * @return
     *     The reposts
     */
    public Reposts getReposts() {
        return reposts;
    }

    /**
     * 
     * @param reposts
     *     The reposts
     */
    public void setReposts(Reposts reposts) {
        this.reposts = reposts;
    }

    /**
     * 
     * @return
     *     The photos
     */
    public Photos getPhotos() {
        return photos;
    }

    /**
     * 
     * @param photos
     *     The photos
     */
    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public List<CopyHistory> getCopy_history() {
        return copy_history;
    }

    public void setCopy_history(List<CopyHistory> copy_history) {
        this.copy_history = copy_history;
    }
}
