package angelectro.com.vktest.content;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ResponseNews {

    private List<Item> items = new ArrayList<Item>();
    private List<Profile> profiles = new ArrayList<Profile>();
    private List<Group> groups = new ArrayList<Group>();
    private String next_from;

    /**
     * @return The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return The profiles
     */
    public List<Profile> getProfiles() {
        return profiles;
    }

    /**
     * @param profiles The profiles
     */
    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    /**
     * @return The groups
     */
    public List<Group> getGroups() {
        return groups;
    }

    /**
     * @param groups The groups
     */
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    /**
     * @return The next_from
     */
    public String getNext_from() {
        return next_from;
    }

    /**
     * @param next_from The next_from
     */
    public void setNext_from(String next_from) {
        this.next_from = next_from;
    }

}
