
package angelectro.com.vktest.content;

import java.util.ArrayList;
import java.util.List;

public class Photos {

    private Integer count;
    private List<Item__> items = new ArrayList<Item__>();

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The items
     */
    public List<Item__> getItems() {
        return items;
    }

    /**
     * 
     * @param items
     *     The items
     */
    public void setItems(List<Item__> items) {
        this.items = items;
    }

}
