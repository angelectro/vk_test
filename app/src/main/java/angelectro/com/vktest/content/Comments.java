
package angelectro.com.vktest.content;


public class Comments {

    private Integer count;
    private Integer can_post;

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The can_post
     */
    public Integer getCan_post() {
        return can_post;
    }

    /**
     * 
     * @param can_post
     *     The can_post
     */
    public void setCan_post(Integer can_post) {
        this.can_post = can_post;
    }

}
