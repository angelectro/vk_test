package angelectro.com.vktest.content;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public class Authorization {

    private String user_id;

    private String expires_in;

    private String access_token;

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getExpires_in ()
    {
        return expires_in;
    }

    public void setExpires_in (String expires_in)
    {
        this.expires_in = expires_in;
    }

    public String getAccess_token ()
    {
        return access_token;
    }

    public void setAccess_token (String access_token)
    {
        this.access_token = access_token;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [user_id = "+user_id+", expires_in = "+expires_in+", access_token = "+access_token+"]";
    }
}
