package angelectro.com.vktest.content;


public class Attachment {

    private String type;
    private Photo photo;

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The photo
     */
    public Photo getPhoto() {
        return photo;
    }

    /**
     * @param photo The photo
     */
    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

}
