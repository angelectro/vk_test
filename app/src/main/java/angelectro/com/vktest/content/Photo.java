
package angelectro.com.vktest.content;


public class Photo {
    private Integer id;
    private Integer album_id;
    private Integer owner_id;
    private Integer user_id;
    private String photo_75;
    private String photo_130;
    private String photo_604;
    private Integer width;
    private Integer height;
    private String text;
    private Integer date;
    private Integer postId;
    private String access_key;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The album_id
     */
    public Integer getAlbum_id() {
        return album_id;
    }

    /**
     * 
     * @param album_id
     *     The album_id
     */
    public void setAlbum_id(Integer album_id) {
        this.album_id = album_id;
    }

    /**
     * 
     * @return
     *     The owner_id
     */
    public Integer getOwner_id() {
        return owner_id;
    }

    /**
     * 
     * @param owner_id
     *     The owner_id
     */
    public void setOwner_id(Integer owner_id) {
        this.owner_id = owner_id;
    }

    /**
     * 
     * @return
     *     The user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     * 
     * @param user_id
     *     The user_id
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     * 
     * @return
     *     The photo_75
     */
    public String getPhoto_75() {
        return photo_75;
    }

    /**
     * 
     * @param photo_75
     *     The photo_75
     */
    public void setPhoto_75(String photo_75) {
        this.photo_75 = photo_75;
    }

    /**
     * 
     * @return
     *     The photo_130
     */
    public String getPhoto_130() {
        return photo_130;
    }

    /**
     * 
     * @param photo_130
     *     The photo_130
     */
    public void setPhoto_130(String photo_130) {
        this.photo_130 = photo_130;
    }

    /**
     * 
     * @return
     *     The photo_604
     */
    public String getPhoto_604() {
        return photo_604;
    }

    /**
     * 
     * @param photo_604
     *     The photo_604
     */
    public void setPhoto_604(String photo_604) {
        this.photo_604 = photo_604;
    }

    /**
     * 
     * @return
     *     The width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The date
     */
    public Integer getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(Integer date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The postId
     */
    public Integer getPostId() {
        return postId;
    }

    /**
     * 
     * @param postId
     *     The post_id
     */
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    /**
     * 
     * @return
     *     The access_key
     */
    public String getAccess_key() {
        return access_key;
    }

    /**
     * 
     * @param access_key
     *     The access_key
     */
    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

}
