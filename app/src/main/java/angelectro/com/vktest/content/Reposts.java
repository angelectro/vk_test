
package angelectro.com.vktest.content;


public class Reposts {

    private Integer count;
    private Integer user_reposted;

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The user_reposted
     */
    public Integer getUser_reposted() {
        return user_reposted;
    }

    /**
     * 
     * @param user_reposted
     *     The user_reposted
     */
    public void setUser_reposted(Integer user_reposted) {
        this.user_reposted = user_reposted;
    }

}
