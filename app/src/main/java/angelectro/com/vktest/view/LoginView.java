package angelectro.com.vktest.view;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public interface LoginView {
    public void showError(Throwable throwable);
    public void showLoading();
    public void hideLoading();
    public void openNewsActivity();
}
