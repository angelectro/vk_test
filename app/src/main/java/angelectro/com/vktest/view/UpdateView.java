package angelectro.com.vktest.view;

import angelectro.com.vktest.content.News;

/**
 * Created by Zahit Talipov on 24.04.2016.
 */
public interface UpdateView {
    public void updateAdapter(News news);
    public void showLoading();
    public void hideLoading();
    public void showError(Throwable throwable);
}
