package angelectro.com.vktest;

import android.app.Application;

/**
 * Created by Zahit Talipov on 23.04.2016.
 */
public class AppDelegate extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Settings.init(this);
    }
}
