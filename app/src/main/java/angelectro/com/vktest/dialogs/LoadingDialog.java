package angelectro.com.vktest.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import angelectro.com.vktest.R;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public class LoadingDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private String TAG = LoadingDialog.class.getSimpleName();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.loading_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(view);
        return builder.create();
    }


    public void show(@NonNull FragmentManager manager) {
        show(manager, TAG);
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        dismiss();
    }
}
