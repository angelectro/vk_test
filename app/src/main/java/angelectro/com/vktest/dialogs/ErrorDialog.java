package angelectro.com.vktest.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by Zahit Talipov on 23.04.2016.
 */
public class ErrorDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private static String TITlE_KEY = "title";
    private static String MESSAGE_KEY = "message";
    private String TAG = ErrorDialog.class.getSimpleName();

    public static ErrorDialog createErrorDialog(Throwable throwable) {
        String title = "Ошибка";
        String message = "Возникла непредвиденная ошибка";
        Class aClass = throwable.getClass();
        if (aClass == HttpException.class) {
            HttpException httpException = (HttpException) throwable;
            switch (httpException.code()) {
                case 401: {
                    title = "Ошибка авторизации";
                    message = "Вы ввели неправильный логин или пароль";
                }
            }
        }
        Bundle bundle = new Bundle();
        bundle.putString(TITlE_KEY, title);
        bundle.putString(MESSAGE_KEY, message);
        ErrorDialog errorDialog = new ErrorDialog();
        errorDialog.setArguments(bundle);
        return errorDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        String title = bundle.getString(TITlE_KEY);
        String message = bundle.getString(MESSAGE_KEY);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ок", this);
        return builder.create();
    }


    public void show(@NonNull FragmentManager manager) {
        show(manager, TAG);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dismiss();
    }
}
