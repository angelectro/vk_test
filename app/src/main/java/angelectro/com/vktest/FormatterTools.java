package angelectro.com.vktest;

import android.text.TextUtils;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Zahit Talipov on 24.04.2016.
 */
public class FormatterTools {

    private static int MAX_LENGTH_HEADER=200;

    public static String getDateString(int date) {
        Date d = new Date((long) date * 1000);
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols() {
            @Override
            public String[] getMonths() {
                return new String[]{"января", "февраля", "марта", "апреля", "мая", "июня",
                        "июля", "августа", "сентября", "октября", "ноября", "декабря"};
            }
        };
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM HH:mm", dateFormatSymbols);
        return dateFormat.format(d);
    }

    public static String headerFormat(String s) {
        if (!TextUtils.isEmpty(s)) {
            if (s.length() > MAX_LENGTH_HEADER) {
                for (int i = MAX_LENGTH_HEADER; i < s.length(); i++) {
                    if (s.charAt(i) == ' ')
                        s = String.format("%s...", s.substring(0, i));
                }
            }
        }
        return s;
    }
}
