package angelectro.com.vktest.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.app.LoaderManager;
import android.text.TextUtils;
import android.util.Log;

import angelectro.com.vktest.BuildConfig;
import angelectro.com.vktest.R;
import angelectro.com.vktest.Settings;
import angelectro.com.vktest.api.ApiProvider;
import angelectro.com.vktest.content.Authorization;
import angelectro.com.vktest.view.LoginView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 22.04.2016.
 */
public class LoginPresenter {
    private static String GRANT_TYPE = "password";
    private LoginView mLoginView;
    private Context mContext;

    public LoginPresenter(LoginView mLoginView) {
        this.mLoginView = mLoginView;
    }



    public void checkIsAuthorized() {
        if (!TextUtils.isEmpty(Settings.getToken())) {
            Log.d("vk", "authorized");
            mLoginView.openNewsActivity();
        }
    }

    public void authorization(@NonNull String login, @NonNull String password) {
        Log.d("login", login);
        ApiProvider.getProviderVKServiceAuthorization()
                .getToken(GRANT_TYPE,
                        BuildConfig.CLIEND_ID,
                        BuildConfig.CLIEND_SECRET,
                        login,
                        password)
                .flatMap(new Func1<Authorization, Observable<Authorization>>() {
                    @Override
                    public Observable<Authorization> call(Authorization authorization) {
                        Settings.save(authorization);
                        return Observable.just(authorization);
                    }
                })
                .doOnSubscribe(mLoginView::showLoading)
                .doOnTerminate(mLoginView::hideLoading)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(authorization -> mLoginView.openNewsActivity(),mLoginView::showError);
    }
}
