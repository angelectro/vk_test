package angelectro.com.vktest.presenter;

import angelectro.com.vktest.api.ApiProvider;
import angelectro.com.vktest.view.UpdateView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zahit Talipov on 24.04.2016.
 */
public class NewsRecyclerPresenter {
    private String FILTERS = "post";

    private UpdateView newsView;

    public NewsRecyclerPresenter(UpdateView newsView) {
        this.newsView = newsView;
    }

    public void refresh() {
        ApiProvider.getProviderVKService()
                .getNews(FILTERS)
                .doOnSubscribe(newsView::showLoading)
                .doOnTerminate(newsView::hideLoading)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newsView::updateAdapter, newsView::showError);
    }
    public void refresh(String startFrom) {

        ApiProvider.getProviderVKService()
                .getNewsNext(startFrom,FILTERS)
                .doOnSubscribe(newsView::showLoading)
                .doOnTerminate(newsView::hideLoading)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newsView::updateAdapter, newsView::showError);
    }


}
